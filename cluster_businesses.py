import json
import sys
from haversine import haversine as h

# Return JSON dictionary
def load_json(filename):
    with open(filename) as f:
        return json.load(f)

# Dictionary with school name as key and tuple(lat, lng) as values
def load_schools():
    schools = load_json("schools.json")
    school_coords = {}
    for s in schools["schools"]:
        school_coords[s["name"]] = (s["location"]["lat"], s["location"]["lng"])
    return school_coords

# Dictionary with business type as key and list of tuple(lat, lng) of those businesses as values
def process_business_data(business_raw, business_type):
    b_coords = []
    for i in range(len(business_raw["results"])):
        b_coords.append((business_raw["results"][i]["geometry"]["location"]["lat"],
                         business_raw["results"][i]["geometry"]["location"]["lng"]))
    return {business_type: b_coords}

# List of tuples with (JSON->Dictionary of businesses, string of business type)
def load_businesses():
    bail_bonds_raw = load_json("json/bail-bonds.json")
    dollar_stores_raw = load_json("json/dollar-stores.json")
    fast_food_raw = load_json("json/fast-food.json")
    laundromats_raw = load_json("json/laundromats.json")
    liquor_stores_raw = load_json("json/liquor-stores.json")
    motels_raw = load_json("json/motels.json")
    pawn_shops_raw = load_json("json/pawn-shops.json")
    project_housing_raw = load_json("json/project-housing.json")
    taqueria_raw = load_json("json/taqueria.json")
    title_loans_raw = load_json("json/title-loans.json")
    used_tires_raw = load_json("json/used-tires.json")

    businesses_raw = [(bail_bonds_raw, "bail-bonds"), (dollar_stores_raw, "dollar-stores"), (fast_food_raw, "fast-food"), (laundromats_raw, "laundromats"),
                      (liquor_stores_raw, "liquor-stores"), (motels_raw, "motels"), (pawn_shops_raw, "pawn-shops"), (project_housing_raw, "project-housing"),
                      (taqueria_raw, "taqueria"), (title_loans_raw, "title-loans"), (used_tires_raw, "used-tires")]
    return businesses_raw

# Dictionary with
# {
#   key = business_type (string),
#   value = dictionary with
#           {
#              key = school name,
#              value = list of tuples of business coordinates (lat, lng)
#           }
# }
def cluster_business_to_schools(business_raw, business_type, schools):
    c = {}
    for s_obj in schools["schools"]:
        c[s_obj["name"]] = []

    for i in range(len(business_raw["results"])):
        b = business_raw["results"][i]
        b_coord = (b["geometry"]["location"]["lat"],
                   b["geometry"]["location"]["lng"])

        closest_school = None
        min_dist = sys.maxsize

        for s_obj in schools["schools"]:
            s_coord = (s_obj["location"]["lat"], s_obj["location"]["lng"])
            haversine_dist = h.haversine(s_coord, b_coord)
            if haversine_dist < min_dist:
                closest_school = s_obj["name"]
                min_dist = haversine_dist
        c[closest_school].append(b_coord)
    return {business_type: c}

def cluster_all_businesses():
    schools = load_json("schools.json")
    businesses_raw = load_businesses()

    # List of dictionaries.
    # Keys are school names, values are list of businesses close to that school
    # Each dictionary represents a different type of business
    cluster_b_to_s = [cluster_business_to_schools(b_raw[0], b_raw[1], schools) for b_raw in businesses_raw]
    return cluster_b_to_s

    # Counts the number of items in each school's dictionary of businesses, over all of the different types of businesses
    # clus = each item in cluster_b_to_s,
    #   which is a dictionary with a
    #              {
    #                   key of business type,
    #                   value of a dictionary with
    #                              {
    #                                   key = school name,
    #                                   value = list of business coordinates
    #                               }
    #              }
    # biz_type is the string key of clus
    # school_cluster_name is the string key of clus[biz_type]. This is the name of the school
    # clus[biz_type][school_cluster_name] is the actual list of business coordinates
    # school_clusters_count = {}
    # for s_obj in schools["schools"]:
    #     school_clusters_count[s_obj["name"]] = 0
    # for clus in cluster_b_to_s:
    #     for biz_type in clus.keys():
    #         for school_cluster_name in clus[biz_type]:
    #             school_clusters_count[school_cluster_name] += len(clus[biz_type][school_cluster_name])
    # print(school_clusters_count)

def biz_school_cluster_to_json(cluster, business_type):
    with open('json/school_clusters/school-' + business_type + '.json', 'w') as outfile:
        json.dump(cluster, outfile)

# Cluster is the output of cluster_business_to_schools
# Calls biz_school_cluster_json to write out json files for all school-business clusters
def write_clusters_to_json(clusters):
    for b_to_s_c in clusters:
        for biz_type in b_to_s_c:
            biz_school_cluster_to_json(b_to_s_c, biz_type)

# cluster_b_to_s = cluster_all_businesses()
# write_clusters_to_json(cluster_b_to_s)