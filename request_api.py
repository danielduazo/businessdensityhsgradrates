import sys
import requests
import json

# url = sys.argv[1]
# query = sys.argv[2]
# key = sys.argv[3]
# out_filename = sys.argv[4]

google_places_url = "https://maps.googleapis.com/maps/api/place/textsearch/json"
key = "AIzaSyBW3_uFH96EHr_teFjXrhBac1tR9gx5tWA"

def get_google_places_api_and_write(url, query, key, out_filename):
    params = dict()
    params["query"] = query
    params["key"] = key

    r = requests.get(url, params)
    data = r.json()
    with open(out_filename, 'w') as outfile:
        json.dump(data, outfile)

get_google_places_api_and_write(google_places_url, "pawn-shops-in-brownsville-texas", key, "json/pawn-shops.json")
get_google_places_api_and_write(google_places_url, "liquor-stores-in-brownsville-texas", key, "json/liquor-stores.json")
get_google_places_api_and_write(google_places_url, "title-loans-in-brownsville-texas", key, "json/title-loans.json")
get_google_places_api_and_write(google_places_url, "fast-food-in-brownsville-texas", key, "json/fast-food.json")
get_google_places_api_and_write(google_places_url, "laundromats-in-brownsville-texas", key, "json/laundromats.json")
get_google_places_api_and_write(google_places_url, "taqueria-in-brownsville-texas", key, "json/taqueria.json")
get_google_places_api_and_write(google_places_url, "motels-in-brownsville-texas", key, "json/motels-stores.json")
get_google_places_api_and_write(google_places_url, "bail-bonds-in-brownsville-texas", key, "json/bail-bonds.json")
get_google_places_api_and_write(google_places_url, "dollar-stores-in-brownsville-texas", key, "json/dollar-stores.json")
get_google_places_api_and_write(google_places_url, "used-tires-in-brownsville-texas", key, "json/used-tires.json")
get_google_places_api_and_write(google_places_url, "project-housing-in-brownsville-texas", key, "json/project-housing.json")