var map;
var geocoder;
var markers = []; // Array that holds the pins/markers of start/end locations

// Function to initialize the maps
function initialize() {
    var myLatLng = {lat: 25.951230, lng: -97.487095}; // Brownsville, Texas (intersection of 802 and Paredes)

    // Set map options
    var mapOpt = {
        center: myLatLng, // Center at myLatLng
        zoom: 15,
        mapTypeId:google.maps.MapTypeId.ROADMAP
    };

    map = new google.maps.Map(document.getElementById('googleMap'), mapOpt); // Create new map object in googleMap div

    // Try HTML5 geolocation to center map at current location
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(function(position) {
            var pos = {
                lat: position.coords.latitude,
                lng: position.coords.longitude
            };
            map.setCenter(pos)
        })
    }

// Bias the autocomplete object to the user's geographical location,
// as supplied by the browser's 'navigator.geolocation' object.
function geolocate() {
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(function(position) {
        var geolocation = {
            lat: position.coords.latitude,
            lng: position.coords.longitude
        };
        var circle = new google.maps.Circle({
            center: geolocation,
            radius: position.coords.accuracy
        });
        autocomplete.setBounds(circle.getBounds());
        });
    }
}

function display_pins_on_map(json) {
	// Load JSON school-business clusters and display on Google Maps with pins
}

google.maps.event.addDomListener(window, 'load', initialize); // Initialize map and forms on load
